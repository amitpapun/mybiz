package com.mybiz.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
@SpringBootApplication
@OpenAPIDefinition
@EnableWebSecurity
@EnableGlobalAuthentication
@EnableFeignClients
public class MyBizAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(MyBizAppApplication.class, args);
	}

}
