package com.mybiz.app.service.impl;

import java.util.Objects;

import com.mybiz.app.entity.User;
import com.mybiz.app.model.UserRoleDetails;
import com.mybiz.app.repository.UserRepo;
import com.mybiz.app.service.UserProjectService;
import com.mybiz.app.service.UserRoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mybiz.app.entity.UserDetails;
import com.mybiz.app.model.GenericResponse;
import com.mybiz.app.model.LoginCredentials;
import com.mybiz.app.model.SignUpDetails;
import com.mybiz.app.repository.UserActionDetailsRepo;
import com.mybiz.app.service.UserActionService;

@Service
public class UserActionServiceImpl implements UserActionService {
    @Autowired
	private UserActionDetailsRepo userActionDetailsRepo;

	@Autowired
	private UserProjectService userProjectService;

	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private UserRepo userRepo;

	@Override
	public GenericResponse validateLoginDetails(LoginCredentials loginCredentials) {
		GenericResponse genericResponse = new GenericResponse();
		UserDetails userDetails = userActionDetailsRepo.findByUserName(loginCredentials.getUserName());
		if(Objects.nonNull(userDetails)) {
			if (StringUtils.equals(loginCredentials.getPassword(), userDetails.getPassword())) {
				genericResponse.setMessage("success");
				genericResponse.setStatusCode("200");
				return genericResponse;
			}
		}
		genericResponse.setMessage("failure");
		genericResponse.setStatusCode("401");
		return genericResponse ;
	}

	@Override
	public GenericResponse userSignUp( SignUpDetails signUpDetails) {
		GenericResponse genericResponse = new GenericResponse();
		/*UserDetails userDetails = new UserDetails();
		BeanUtils.copyProperties(signUpDetails, userDetails);
		userActionDetailsRepo.save(userDetails);*/
		User user = getUser(signUpDetails);
		userRepo.save(user);
		genericResponse.setMessage("Success");
		genericResponse.setStatusCode("200");
		return genericResponse;
	}

	private User getUser(SignUpDetails signUpdetails) {
		User user = new User();
		user.setUsername(signUpdetails.getUserName());
		user.setPassword(signUpdetails.getPassword());
		user.setEnable(1);
		return user;
	}

	@Override
	public GenericResponse refresh(String userId) {
		UserRoleDetails userRoleDetails = userRoleService.getUserRoleDetails(userId);
		return null;
	}

}
