package com.mybiz.app.service.impl;

import com.mybiz.app.client.UserRoleClient;
import com.mybiz.app.model.UserRoleDetails;
import com.mybiz.app.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleClient userRoleClient;

    @Override
    public UserRoleDetails getUserRoleDetails(String userId) {
        return userRoleClient.fetchUserRoleDetails(userId);
    }
}
