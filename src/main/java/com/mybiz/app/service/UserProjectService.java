package com.mybiz.app.service;

import com.mybiz.app.model.UserProjectDetails;
import org.springframework.stereotype.Service;

@Service
public interface UserProjectService {
    UserProjectDetails getUserProjectDetails();
}
