package com.mybiz.app.service;

import com.mybiz.app.model.UserRoleDetails;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public interface UserRoleService {
    UserRoleDetails getUserRoleDetails(String userId);
}
