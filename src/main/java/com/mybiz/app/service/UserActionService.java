package com.mybiz.app.service;

import javax.validation.Valid;

import com.mybiz.app.entity.User;
import org.springframework.stereotype.Service;

import com.mybiz.app.model.GenericResponse;
import com.mybiz.app.model.LoginCredentials;
import com.mybiz.app.model.SignUpDetails;
@Service
public interface UserActionService {
    GenericResponse validateLoginDetails(LoginCredentials loginCredentials);
    GenericResponse userSignUp(@Valid SignUpDetails signUpdetails);
    GenericResponse refresh(String userId);
}
