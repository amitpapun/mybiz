package com.mybiz.app.controller;

import com.mybiz.app.model.GenericResponse;
import com.mybiz.app.model.LoginCredentials;
import com.mybiz.app.model.SignUpDetails;
import com.mybiz.app.service.UserActionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserActionController {

    @Autowired
    private UserActionService userActionService;

    @Operation(summary = "This is for user login")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = " user successfully logged in", content = {@Content(mediaType = "application/json")}), @ApiResponse(responseCode = "404", description = " Page Not Found", content = @Content)})
    @PostMapping("/signin")
    public GenericResponse login(@RequestBody LoginCredentials loginCredential) {
        return userActionService.validateLoginDetails(loginCredential);
    }

    @Operation(summary = "This is to add  the user in the database")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = " user details saved in database", content = {@Content(mediaType = "application/json")}), @ApiResponse(responseCode = "404", description = " Page Not Found", content = @Content)})
    @PostMapping(value = "/signup", consumes = "application/json")
    public GenericResponse signUp(@Valid @RequestBody SignUpDetails signUpDetails) {
        return userActionService.userSignUp(signUpDetails);
    }

    @GetMapping(value = "/refreshUserDetails/{userId}")
    public GenericResponse refresh(@PathVariable String userId) {
        return userActionService.refresh(userId);
    }
}
