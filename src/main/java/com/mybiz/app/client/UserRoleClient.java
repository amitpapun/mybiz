package com.mybiz.app.client;

import com.mybiz.app.model.UserRoleDetails;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "UserRoleDetails", url = "http://localhost:8083")
public interface UserRoleClient {
    @GetMapping("/getUserProfileDetails/{userId}")
    UserRoleDetails fetchUserRoleDetails(@PathVariable String userId);
}
