package com.mybiz.app.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericResponse {
	private String statusCode;
	private String message;
}
