package com.mybiz.app.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class UserRoleDetails {
    private Integer userId;
    private String department;
    private String designation;
    private String salary;
    private String primarySkill;
    private String secondarySkill;
    private Date dateOfJoining;
    private boolean status;
}
