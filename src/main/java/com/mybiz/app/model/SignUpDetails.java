package com.mybiz.app.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
public class SignUpDetails {
	private String name;
	private String userName;
	private String password;
	private Address address;
	final static class Address {
		private String city;
		private Integer pinCode;
	}
	@NotNull(message = "Invalid input")
	@NotBlank(message = "The city is required.")
	private String phoneNumber;
	@Email(message = "Invalid email format")
	private String emailId;
}
