package com.mybiz.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class UserDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private Integer userId;
	private String name;
	@Column(name = "userName")
	private String userName;
	private String password;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Column(name = "email_id")
	private String emailId;
	@Column(name = "project_name")
	private String projectName;
	@Column(name = "user_role")
	private String userRole;
	@Column(name = "user_designation")
	private String userDesignation;
}
