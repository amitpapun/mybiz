package com.mybiz.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mybiz.app.entity.UserDetails;

@Repository
public interface UserActionDetailsRepo extends JpaRepository<UserDetails, Integer>{
	UserDetails findByUserName( String userName);
}
